
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , nowjs = require('now')
  , uaParser = require('ua-parser');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes
var sys = require('sys');

app.get('/', routes.index);
app.get('/join', routes.join);

app.listen(3000);
var everyone = require("now").initialize(app);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

var responseHistory = new Array();
var clientData = new Array();

function createResponseList(userGroup, sid, response, clientId) {
  if (typeof responseHistory[userGroup] == 'undefined') {
    responseHistory[userGroup] = new Array();
  }
  if (typeof responseHistory[userGroup][sid] == 'undefined') {
    responseHistory[userGroup][sid] = {'count': 0, 'responses': {'data': new Array(), 'clientId': clientId}};
  }
  var resExists = false;
  responseHistory[userGroup][sid].count++;
    for (res in responseHistory[userGroup][sid].responses.data) {
      if (responseHistory[userGroup][sid].responses.data[res] == response) {
        resExists = true;
        break;
      } 
    }
  if (!resExists) responseHistory[userGroup][sid].responses.data.push(response);
  var responseList = responseHistory[userGroup][sid].responses.data;
  nowjs.getGroup(userGroup).count(function(count){
    if (count == responseHistory[userGroup][sid].count) {
      responseHistory[userGroup][sid] = undefined;
      if (responseHistory[userGroup].length == 0) {
        responseHistory[userGroup] = undefined;
      }
    } 
  });
  return responseList;
}

everyone.now.distributeMessage = function(message){
  var now = this.now;
  if (message.request == 'join') {
    nowjs.getGroup(message.sessionId).count(function(count){
      var response = null;
      if (!count) {
        response = {'sessionId': message.sessionId, 'response': 'absent'};
      } else {
        response = {'sessionId': message.sessionId, 'response': 'allow'};
      }
      now.receiveMessage('jsconsole', response);
    });
  } else if (message.request == 'start') {
    nowjs.getGroup(message.sessionId).addUser(this.user.clientId);
    var ua = uaParser.parse(message.userAgent);
    clientData[this.user.clientId] = {'browser': ua.family, 'version': ua.toVersionString(), 'os': ua.os};
    var response = {'sessionId': message.sessionId, 'response': 'ready'};
    everyone.now.receiveMessage('jsconsole', response);
  } else {
    var response = null;
    this.getGroups(function(groups){
      var userGroup = groups[1];
      nowjs.getGroup(userGroup).now.execute(message.command, function(error, result) {
        if (error != null) {
          response = 'Error: ' + error;
        } else {
          response = result;
        }
        var responseList = createResponseList(userGroup, message.sid, response, this.user.clientId);
        var responseMessage = {'sid': message.sid, 'response': responseList};
        now.receiveMessage('jsconsole', responseMessage);
      });
    });
  }
};