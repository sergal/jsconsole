
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: "Express", scripts: ['/javascripts/jquery.min.js', '/javascripts/request.js', '/nowjs/now.js'] });
};

exports.join = function(req, res){
  res.render('join', { title: "Express", scripts: ['/javascripts/jquery.min.js', '/javascripts/join.js', '/nowjs/now.js'] });
};