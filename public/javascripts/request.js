$(document).ready(function(){
    var commandId = 0;
    var ready = false;
    var sessionId = randomString(12);
    $('#sessionId').html(sessionId);
    $('#submit').click(function(){
        var command = {'sid': commandId, 'command': $('#command').val()};
        now.distributeMessage(command);
        $('#response').append('<li id="' + commandId + '">' + $('#command').val() + '</li>');
        $('#command').val('');
        commandId++;
    });

    now.receiveMessage = function(name, message){
        if (!ready) {
            if ((message.response == 'ready') && (message.sessionId == sessionId)) ready = true;
        } else {
            var sid = message.sid;
            if (typeof sid != 'undefined') {
                $('#response_' + sid).remove();
                $('li#'+sid).after('<ul id="response_' + sid + '"></ul>');
                for (var item in message.response) {
                    $('#response_' + sid).append('<li>' + message.response[item] + '</li>');
                }
            }
        }
    }

    now.ready(function() {
        now.name = 'jsconsole';
        var message = {'request': 'start', 'sessionId': sessionId, 'userAgent': navigator.userAgent};
        now.distributeMessage(message);
        now.execute = function(command, callback) {
            try {
                var result = eval(command);
                callback(null, result);
            } catch (e) {
                var error = e.message;
                callback(error, null);
            }
        }
    });
});

function randomString(length) {
    var chars = '0123456789abcdefghiklmnopqrstuvwxyz'.split('');
    
    if (!length) {
        length = Math.floor(Math.random() * chars.length);
    }
    
    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}