$(document).ready(function(){
    var parameters = getUrlVars();
    var sessionId = parameters['sessionId'];


    now.ready(function() {
        now.name = 'jsconsole';
        if (typeof sessionId == 'undefined') {
            $('#inputDiv').css('display', 'block');
        } else {
            sendJoinMessage();
        }

    });

    now.receiveMessage = function(name, message) {
        if (message.sessionId == sessionId) {
            if (message.response == 'absent') {
                alert('There are no session with this sessionID on the server');
                $('#inputDiv').css('display', 'block');
            } else if (message.response == 'allow') {
                var isConfirm = confirm('Allow the server to run commands in your browser?');
                if (isConfirm) {
                    var message = {'request': 'start', 'sessionId': sessionId, 'userAgent': navigator.userAgent};
                    now.distributeMessage(message);
                }
            }
        }
    }

    $('#submit').live('click', function() {
        sessionId = $('#commandId').val();
        $('#commandId').val('');
        $('#inputDiv').css('display', 'none');
        sendJoinMessage();
    });

    function sendJoinMessage(){
        var message = {'request': 'join', 'sessionId': sessionId};
        now.distributeMessage(message);
        now.execute = function(command, callback) {
            try {
                var result = eval(command);
                callback(null, result);
            } catch (e) {
                var error = e.message;
                callback(error, null);
            }
        }        
    }
});

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}